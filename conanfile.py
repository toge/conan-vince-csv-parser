from conans import ConanFile, tools
import shutil

class CsvparserConan(ConanFile):
    name           = "vince-csv-parser"
    license        = "MIT"
    author         = "toge.mail@gmail.com"
    url            = "https://bitbucket.org/toge/conan-vince-csv-parser/"
    homepage       = "https://github.com/vincentlaucsb/csv-parser"
    description    = "A modern C++ library for reading, writing, and analyzing CSV (and similar) files."
    settings       = "os"
    topics         = ("header only", "csv")
    generators     = "cmake"
    no_copy_source = True

    def source(self):
        # tools.get("https://github.com/vincentlaucsb/csv-parser/archive/{}.zip".format(self.version))
        tools.get(**self.conan_data["sources"][self.version])
        shutil.move("csv-parser-{}".format(self.version), "csv-parser")

    def package(self):
        self.copy("*.hpp", dst="include", src="csv-parser/single_include")

    def package_info(self):
        self.info.header_only()
        if self.settings.os == "Linux":
            self.cpp_info.libs = ["dl", "m", "pthread"]
