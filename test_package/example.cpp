#include <iostream>
#include "csv.hpp"

int main() {
    csv::CSVFormat format;
    format.delimiter('\t')
        .quote('~')
        .header_row(2);
}
